// Vouchers Model

const {vouchers} = require('../data.json');

// Retourne la liste de tous les coupons disponibles
const getVouchers = () => {
    return vouchers;
}
// Retourne le montant de réduction d'un coupon en le recherchant par son nom
const getVoucherValueByName = (name) => {
    const voucher = vouchers.find(voucher => { // On parcourt la liste des coupons
        return Object.keys(voucher).find(key => { // On parcourt la liste des attributs d'un coupon (le nom du coupon est dans l'attribut)
            return key === name;
        });
    });

    return voucher[name]; // Retourner uniquement le montant de la réduction
}

module.exports = {
    getVouchers,
    getVoucherValueByName,
}
